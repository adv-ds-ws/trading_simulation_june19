import requests
import re
import pandas as pd


def load_headline_text_from_www(page_num=1):
    result = requests.get(f"https://www.nasdaq.com/news/market-headlines.aspx?page={page_num}")
    return result.content.decode("utf-8")

def load_stock_sentiment_from_www(stock_ticker):
    result = requests.get(f"https://community.nasdaq.com/community-ratings.aspx?stockticker={stock_ticker}")
    return result.content.decode("utf-8")

def return_words_in_text(text, watchlist, max_hits=3):
    found_on_page = []
    words = re.split("\W+", text)
    unique_words = list(set(words))
    for word in unique_words:
        if word.lower() in watchlist:
            found_on_page.append(word.lower())
        if len(found_on_page) == max_hits:
            break
    return found_on_page

def search_headline_pages_for_stock_mentions(watchlist, max_hits=3, max_pages_to_search=3):
    found_in_headlines = []
    for page_num in range(max_pages_to_search):
        text = load_headline_text_from_www(page_num + 1)
        found_on_page = return_words_in_text(text, watchlist, max_hits - len(found_in_headlines))
        found_in_headlines.extend(found_on_page)
        if len(found_in_headlines) == max_hits:
            break
    return found_in_headlines

def get_tip_for_stock(stock_ticker: str) -> str:
    """Returns sell or buy recommendation for ticker or 'dont know' is recommendation is unavailable."""
    sentiment_txt = load_stock_sentiment_from_www(stock_ticker)
    if '''class="bullRating">Bullish''' in sentiment_txt:
        rating = "buy"
    elif '''class="bearRating">Bearish''' in sentiment_txt:
        rating = "sell"
    else:
        #       rating = "dont know"
        rating = None
    return rating

def get_stock_tips(max_hits, max_pages_to_search=3) -> (str, str):
    """Returns list of tuples with ticker and recommendation."""
    tips = []
    stock_watchlist = load_stock_watchlist()
    stock_mentions = search_headline_pages_for_stock_mentions(stock_watchlist, max_hits, max_pages_to_search)
    for stock_mention in stock_mentions:
        tip = get_tip_for_stock(stock_mention)
        #      if tip != "dont know":
        if (tip is not None):
            tips.append((stock_mention, tip))
    return tips


def load_stock_watchlist(path="stock_watchlist.csv"):
    stock_watchlist = pd.read_csv(path)
    return [w.lower() for w in stock_watchlist["stocks"]]
