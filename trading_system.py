import copy
import pandas as pd

def record_trade_in_portfolio(ticker, price, volume, buy, portfolio):
    """changes portfolio dict in place to record trade. Adjusts position of cash and ticker traded."""

    if not buy:
        volume = -volume
    if portfolio.get(ticker, 0) == 0:
        portfolio[ticker] = volume
        portfolio["cash"] = portfolio["cash"] - (volume * price)
    else:
        portfolio[ticker] += volume
        portfolio["cash"] = portfolio["cash"] - (volume * price)
    return portfolio

class TradingSystem:
    """Interface to buy and sell positions in portfolio."""
    def __init__(self, portfolio: {}, market_api, risk_api=None):
        self.portfolio = copy.deepcopy(portfolio)
        self.market_api = market_api
        self.risk_api = risk_api

    def get_portfolio_position(self, ticker):
        return self.portfolio.get(ticker, 0)

    def trade(self, ticker, volume, buy=True) -> (bool, str):
        """Attempts to buy or sell, returns tuple with success flag and explanatory message in case of failure."""
        """ trading_sys.trade("googl", 1000_000_000, buy=True)"""

        vwap_price = self.calculate_market_vwap(ticker, volume, buy)
        if vwap_price is not None:
            if self.risk_api.is_trade_within_limit(ticker, vwap_price, volume, buy, self.portfolio):
                confirmed, message = self.market_api.execute_order(ticker, volume)
                if confirmed:
                    record_trade_in_portfolio(ticker, vwap_price, volume, buy, self.portfolio)
                    return confirmed, message
                else:
                    return confirmed, message
            else:
                return False, "Trade would break limit!"
        else:
            return False, "Not enough market volume!"

    @staticmethod
    def calculate_vwap(book: pd.DataFrame, volume: float, buy: bool = True) -> float:
        """Calculate volume weighted average price (vwap) given a market book holding positions.
        Returns null if book holds insufficient volume to calculate vwap for desired volume."""
        
        if buy == True:
            sorted_book = book[book["side"] == 'ASK'].sort_values(['price'], ascending = True)
           
            
        else:
            sorted_book = book[book["side"] == 'BID'].sort_values(['price'], ascending = False)
           
        
        sorted_book["cumsum_volume"] = sorted_book["volume"].cumsum()
        max_volume = float(sorted_book['cumsum_volume'].tail(1).values)
        
        if volume > max_volume:
            return None

        else:        
        
            sorted_book = sorted_book.reset_index()
            idx_cumsum = sorted_book[sorted_book["cumsum_volume"] >= volume].iloc[0:1:1].index[0]

            sorted_book.iloc[idx_cumsum, sorted_book.columns.get_loc('volume')] = sorted_book.iloc[idx_cumsum,:]['volume'] - (sorted_book.iloc[idx_cumsum,:]['cumsum_volume'] - volume)
            sorted_book = sorted_book.iloc[0:idx_cumsum+1,:]
            sorted_book["product_price_volume"] = sorted_book["volume"] * sorted_book["price"]
            sorted_book['cumsum_product_price_volume'] = sorted_book["product_price_volume"].cumsum() 
            vwap = sorted_book['cumsum_product_price_volume'].tail(1)
            vwap = float(vwap.values) / volume


        return vwap

    def calculate_market_vwap(self, ticker, volume, buy=False) -> float:
        """Calculate volume weighted average price (vwap) for ticker."""
        df = self.market_api.get_market_book(ticker)
        result = self.calculate_vwap(df, volume, buy)
        return result 
