import trading_system
from market_api import MarketAPI
from historic_data import HistoricData
from risk_system import RiskSystem
import pytest
import pandas as pd
import copy


@pytest.fixture(scope="function")
def trading_sys():
    pf = {"cash": 100,
          "googl": 235,
          "aapl": -15}
    market = MarketAPI(historic_data=HistoricData)
    risk_sys = RiskSystem(historic_data=HistoricData)
    return trading_system.TradingSystem(portfolio=pf, market_api=market, risk_api=risk_sys)


def test_get_portfolio_position(trading_sys):
    assert trading_sys.get_portfolio_position("googl") == 235
    assert trading_sys.get_portfolio_position("xyz") == 0


def test_trade(trading_sys, mocker):
    mocker.patch("market_api.MarketAPI.execute_order", return_value=(True, "OK"))

    ok, msg = trading_sys.trade("googl", 1000_000_000, buy=True)
    assert (ok, msg) == (False, "Not enough market volume!")


    trading_sys.risk_api.var_limit = 0
    ok, msg = trading_sys.trade("googl", 100, buy=True)
    assert (ok, msg) == (False, "Trade would break limit!")

    trading_sys.risk_api.var_limit = 1000_000_000
    ok, msg = trading_sys.trade("googl", 100, buy=True)
    assert (ok, msg) == (True, "OK")

def test_trade1(trading_sys, mocker):
    mocker.patch("trading_system.TradingSystem.calculate_market_vwap", return_value=None)
    mocker.patch("risk_system.RiskSystem.is_trade_within_limit", return_value=True)
    ok, msg = trading_sys.trade("googl", 1000_000_000, buy=True)
    assert (ok, msg) == (False, "Not enough market volume!")
    #    mocker.patch("market_api.MarketAPI.execute_order", return_value=(True, "OK"))



def test_calculate_vwap(trading_sys):

    book = pd.DataFrame({
        "side": ["ASK", "ASK", "ASK", "ASK",
                 "BID", "BID", "BID"],
        "price": [102, 97, 96, 94,
                  92, 90, 87],
        "volume": [3, 5, 2, 2,
                   1, 2, 5]})
    vwap_buy_1 = trading_sys.calculate_vwap(book, 1, buy=True)
    vwap_buy_2 = trading_sys.calculate_vwap(book, 2, buy=True)
    vwap_buy_4 = trading_sys.calculate_vwap(book, 4, buy=True)
    vwap_buy_6 = trading_sys.calculate_vwap(book, 6, buy=True)
    vwap_buy_7 = trading_sys.calculate_vwap(book, 7, buy=True)
    vwap_buy_12 = trading_sys.calculate_vwap(book, 12, buy=True)

    vwap_sell_1 = trading_sys.calculate_vwap(book, 1, buy=False)
    vwap_sell_2 = trading_sys.calculate_vwap(book, 2, buy=False)
    vwap_sell_3 = trading_sys.calculate_vwap(book, 3, buy=False)
    vwap_sell_7 = trading_sys.calculate_vwap(book, 7, buy=False)
    vwap_sell_100 = trading_sys.calculate_vwap(book, 100, buy=False)

    assert vwap_buy_1 == 94
    assert vwap_buy_2 == 94
    assert vwap_buy_4 == (94 * 2 + 96 * 2) / 4
    assert vwap_buy_6 == (94 * 2 + 96 * 2 + 97 * 2) / 6
    assert vwap_buy_7 == (94 * 2 + 96 * 2 + 97 * 3) / 7
    assert vwap_buy_12 == (94 * 2 + 96 * 2 + 97 * 5 + 102 * 3) / 12

    assert vwap_sell_1 == 92
    assert vwap_sell_2 == (92 * 1 + 90 * 1) / 2
    assert vwap_sell_3 == (92 * 1 + 90 * 2) / 3
    assert vwap_sell_7 == (92 * 1 + 90 * 2 + 87 * 4) / 7
    assert vwap_sell_100 is None

def test_record_trade_in_portfolio():
    pf = {"cash": 100,
          "googl": 235,
          "aapl": -15}

    assert trading_system.record_trade_in_portfolio("googl", 100, 10, True, copy.deepcopy(pf))["googl"] == 245
    assert trading_system.record_trade_in_portfolio("googl", 100, 10, True, copy.deepcopy(pf))["cash"] == -900
    assert trading_system.record_trade_in_portfolio("ibm", 100, 10, False, copy.deepcopy(pf))["cash"] == 1100
    assert trading_system.record_trade_in_portfolio("ibm", 100, 10, False, copy.deepcopy(pf))["ibm"] == -10