from risk_system import RiskSystem
from historic_data import HistoricData
import pytest
import numpy as np


@pytest.fixture(scope="function")
def risk_sys():
    return RiskSystem(historic_data=HistoricData)


def test_simulate_trade(risk_sys):
    pf = {"cash": 100,
          "googl": 235,
          "aapl": -15}

    pf_after_frade = risk_sys.simulate_trade("aapl", 50, 2, buy=True, portfolio=pf)
    assert pf_after_frade["aapl"] == -13
    assert pf_after_frade["cash"] == 0
    assert pf["aapl"] == -15
    assert pf["cash"] == 100


def test_check_limit(risk_sys):
    pf = {"cash": 500,
          "aapl": 3}

    risk_sys.var_limit = risk_sys.simulate_var_for_portfolio(pf)
    limit_intact = risk_sys.is_trade_within_limit("aapl", 50, 10, buy=True, portfolio=pf)
    assert limit_intact == False


def test_check_limit2(risk_sys, mocker):
    pf = {"cash": 500,
          "aapl": 3}

    risk_sys.var_limit = 2
    mocker.patch("risk_system.RiskSystem.simulate_var_for_portfolio", return_value=3)
    limit_intact = risk_sys.is_trade_within_limit("aapl", 100, 10, True, pf)
    assert limit_intact == False


def test_check_limit3(risk_sys, mocker):
    pf = {"cash": 500,
          "aapl": 3}

    risk_sys.var_limit = 10
    mocker.patch("risk_system.RiskSystem.simulate_var_for_portfolio", return_value=3)
    limit_intact = risk_sys.is_trade_within_limit("aapl", 100, 10, True, pf)
    assert limit_intact == True


def test_get_pf_values_without_cash(risk_sys):
    price_aapl = HistoricData.get_last_price("aapl")
    price_goog = HistoricData.get_last_price("googl")
    pf = {"cash": 100,
          "googl": 235,
          "aapl": -15}

    values = risk_sys._get_pf_values_without_cash(pf)
    assert values == [235 * price_goog, -15 * price_aapl]
    assert risk_sys._get_pf_total_value_without_cash(pf) == sum(values)


def test_calculate_var(risk_sys):
    risk_sys.var_perc = .5
    future_values = np.array([0, 0, -50, -50])
    var = risk_sys._calculate_var(future_values)
    assert var == 50


def test_simulation():
    np.random.seed(23)
    n_sims = 1000
    days = 365
    vola = 0.1
    growth = 0
    risk_sys = RiskSystem(historic_data=HistoricData,
                          growth_rate_year=growth,
                          return_vola_year=vola,
                          var_days=days,
                          num_sims=n_sims)
    pf = {"cash": 1000,
          "googl": 50}

    sim_values = risk_sys._simulate_portfolio_returns_paths(portfolio=pf)
    assert sim_values.shape == (len(pf)-1, n_sims, days) #(len(pf)-1, days, n_sims)
    future_vals = risk_sys._calculate_future_asset_values(sim_values=sim_values, portfolio=pf)
    sd = np.std(future_vals)
    mean = np.mean(future_vals)
    price_goog = HistoricData.get_last_price("googl")
    assert 50 * price_goog * .90 < mean < 50 * price_goog * 1.10
    expected_abs_vola = vola * 50 * price_goog
    assert expected_abs_vola*.90 < sd < expected_abs_vola*1.10

    pf_big = {"cash": 0,
              "googl": 70,
              "amzn": 30,
              "aapl": 100}
    sim_values = risk_sys._simulate_portfolio_returns_paths(portfolio=pf_big)
    assert sim_values.shape == (len(pf_big)-1, n_sims, days) #(len(pf_big)-1, days, n_sims)
    future_vals_big = risk_sys._calculate_future_asset_values(sim_values=sim_values, portfolio=pf_big)
    price_initial = risk_sys._get_pf_total_value_without_cash(pf_big)
    print(future_vals_big)
    print(price_initial)
    #assert price_initial * .90 < np.mean(future_vals_big) < price_initial * 1.10


def test_var():
    # TODO: What if var is calculated for empty portfolio?
    np.random.seed(23)
    n_sims = 1000
    days = 365
    vola = 0.1
    growth = 0
    risk_sys = RiskSystem(historic_data=HistoricData,
                          growth_rate_year=growth,
                          return_vola_year=vola,
                          var_days=days,
                          num_sims=n_sims)
    pf_small = {"cash": 0,
                "googl": 50,
                "aapl": 150}
    var_small = risk_sys.simulate_var_for_portfolio(pf_small)
    pf_big = {"cash": 0,
              "googl": 100,
              "aapl": 300}
    var_big = risk_sys.simulate_var_for_portfolio(pf_big)
    assert var_big > var_small
    