from trading_system import TradingSystem
from market_api import MarketAPI
from risk_system import RiskSystem
from historic_data import HistoricData
from web_news_analyzer import get_stock_tips


def test_workflow():
    pf = {"cash": 10_000}

    market_sys = MarketAPI()
    risk_sys = RiskSystem(historic_data=HistoricData)
    risk_sys.var_limit = 10_000_000
    trading_sys = TradingSystem(market_api=market_sys, risk_api=risk_sys, portfolio=pf)
    
    c = 0
    status = False
    while not status and c < 10:
        assert "googl" not in trading_sys.portfolio
        assert trading_sys.portfolio["cash"] == 10_000

        status, msg = trading_sys.trade("googl", 10, buy=True)
        c += 1

    assert "googl" in trading_sys.portfolio
    assert trading_sys.portfolio["cash"] < 10_000
    var1 = risk_sys.simulate_var_for_portfolio(trading_sys.portfolio)
    cash1 = trading_sys.portfolio["cash"]

    tips = get_stock_tips(max_hits=1, max_pages_to_search=10)
    if tips:
        (tip_ticker, tip_direction) = tips[0]
        status, msg = trading_sys.trade(tip_ticker, 100, buy=tip_direction == "buy")
        var2 = risk_sys.simulate_var_for_portfolio(trading_sys.portfolio)
        if status:
            assert var2 > var1
            assert cash1 != trading_sys.portfolio["cash"]
        else:
            assert var2 == var1
            assert cash1 == trading_sys.portfolio["cash"]
