import web_news_analyzer


def test_load_stock_watchlist(tmp_path):
    tmp_dir = tmp_path / "sub"
    tmp_dir .mkdir()
    test_csv_path = tmp_path / "test_stock_watchlist.txt"
    test_csv_path.write_text("stocks\nIRWD\nM\nR""")
    csv_content = web_news_analyzer.load_stock_watchlist(test_csv_path)
    assert csv_content == ["irwd", "m", "r"]


def get_fake_headlines(page_num):
    if page_num == 1:
        return "IBM blblb GOOGL bla BLUBB"
    elif page_num == 2:
        return "TSLA, buy sell blubb TI"
    else:
        return "bla bla cisq AMD"


def test_return_words_in_text():
    watchlist = ["aapl", "goog", "amd"]
    text = "bla, aapl, blubb goog blabbediblubb bu amd ba blopp!"
    found = web_news_analyzer.return_words_in_text(text, watchlist, max_hits=3)
    assert set(found) == set(["aapl", "goog", "amd"])

    watchlist = ["aapl", "goog", "xxx"]
    text = "bla, AAPL, goog goog xxxblopp!"
    found = web_news_analyzer.return_words_in_text(text, watchlist, max_hits=3)
    assert set(found) == set(["aapl", "goog"])

    watchlist = ["aapl", "goog", "xxx"]
    text = "aapl,goog,amd!"
    found = web_news_analyzer.return_words_in_text(text, watchlist, max_hits=2)
    assert set(found).issubset(watchlist)
    assert len(found) == 2


def test_search_headline_pages_for_stock_mentions(mocker):
    mocker.patch("web_news_analyzer.load_headline_text_from_www", get_fake_headlines)
    watchlist = ["ibm", "googl", "tsla", "ti", "cisq", "amd"]
    mentions = web_news_analyzer.search_headline_pages_for_stock_mentions(watchlist, max_hits=10, max_pages_to_search=3)
    assert set(mentions) == set(watchlist)


def test_get_tip_for_stock(mocker):
    bull_str = """
    ...
    <div id="community-rating">
						<h3>Community Rating:</h3>
						<p>
							<img id="ctl00_ContentPlaceHolder_commRatingimg" src="/common/images/bullish-icon.png" /><br />
							<span id="ctl00_ContentPlaceHolder_commRating" class="bullRating">Bullish</span>
						</p>
						...
    """
    mocker.patch("web_news_analyzer.load_headline_text_from_www", return_value=bull_str)
    tip = web_news_analyzer.get_tip_for_stock("aapl")
    assert tip == "buy"


def test_get_stock_tips(mocker):
    def get_fake_stock_tip(ticker):
        if ticker == "ibm":
            return "buy"
        elif ticker == "tsla":
            return "sell"
        else:
            return None
    mocker.patch("web_news_analyzer.get_tip_for_stock", get_fake_stock_tip)
    mocker.patch("web_news_analyzer.load_headline_text_from_www", get_fake_headlines)
    mocker.patch("web_news_analyzer.load_stock_watchlist", return_value=["ibm", "tsla", "amd"])
    tips = web_news_analyzer.get_stock_tips(max_hits=3, max_pages_to_search=3)
    assert set(tips) == set([("ibm", "buy"), ("tsla", "sell")])
