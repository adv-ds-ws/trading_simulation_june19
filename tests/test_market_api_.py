import market_api
import pytest


@pytest.fixture(scope="module")
def market():
    return market_api.MarketAPI()


def test_calculate_book(market):
    book = market._calculate_book(ticker="GOOG")
    assert max(book[book["side"] == "BID"]["price"]) < min(book[book["side"] == "ASK"]["price"])


def test_ticker_format_check(market):
    wrong_ticker = "§GOOG"
    with pytest.raises(ValueError) as error:
        market.get_market_book(ticker=wrong_ticker)
        assert str(error.value) == f"Ticker '{wrong_ticker}' invalid! Must be alphanumeric."


def test_execute_order(market):
    # trades should not fail twice in a row!
    for _ in range(1000):
        status1, msg1 = market.execute_order(ticker=1000, volume=1000)
        status2, msg2 = market.execute_order(ticker=1000, volume=1000)
        success1 = (status1, msg1) == (True, "OK")
        success2 = (status2, msg2) == (True, "OK")
        assert success1 or success2
