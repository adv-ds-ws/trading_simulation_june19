import historic_data


def test_get_last_price():
    price = historic_data.HistoricData.get_last_price("aapl")
    assert price == 211.75
